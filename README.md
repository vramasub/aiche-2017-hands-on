# README

## About

This is an example project for the execution of a basic compuational workflow with [HOOMD-blue](https://glotzerlab.engin.umich.edu/hoomd-blue/), [signac](http://www.signac.io), and [freud](https://glotzerlab.engin.umich.edu/freud) for the *Hands-On with Molecular Simulations* session presented at the AIChE Annual Meeting 2017 in Minneapolis, MN.

## Requirements

The examples requires Python version >= 2.7.x (Python version >= 3.4 is recommended), and the installation of the following Python packages:

 * hoomd
 * signac
 * freud
 * gsd
 * numpy
 * matplotlib

## Installation of requirements with conda

To install these packages with [conda](https://conda.io), please add the Glotzer and the conda-forge channel to your default channels:
```
$ conda config --add channels glotzer
$ conda config --add channels conda-forge
```
The required packages can then be installed with
```
$ conda install hoomd signac freud gsd numpy matplotlib
```

## Quickstart

To run the example as part of this repository, please clone it to your local computer, *e.g.*, with:
```
git clone https://bitbucket.org/csadorf/aiche-2017-hands-on.git
```
Then change into the repository's root directory and start a jupyter notebook:
```
$ cd aiche-2017-hands-on
$ jupyter notebook
```
Finally, open the `index.ipynb` notebook.
This notebook links to other notebooks containing the full example project.
